# Soma readme

## Setup da aplicação em modo de dev

* usar variavel apiDevKey na apiKey do firebase config em src/config/firebase.js

* copiar o conteúdo de android/app/src/google-services-dev.json para android/app/src/google-services.json

## Setup da aplicação em modo de produção

* usar variavel apiProductionKey na apiKey do firebase config em src/config/firebase.js

* copiar o conteúdo de android/app/src/google-services-production.json para android/app/src/google-services.json

## build

* apk foi gerado pelo https://appcenter.ms/