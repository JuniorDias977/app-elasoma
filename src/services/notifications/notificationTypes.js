export default {
  EVENTS: 'EventNotification',
  NEW_FORUM_POST: 'NewForumPostNotification',
  FORUM_POST_REPLY: 'ForumPostReplyNotification',
};
