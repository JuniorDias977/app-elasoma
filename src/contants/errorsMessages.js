export default {
  'auth/email-already-in-use':
    'O email já está em uso. Tente realizar o login:',
  'auth/user-not-found': 'Usuário não encontrado. Você pode registrar-se',
  'auth/wrong-password': 'Dados inválidos, tente novamente',
  'firestore/permission-denied':
    'Você não tem permissão para realizar este tipo de operação',
};

export const logoutFlags = ['firestore/permission-denied'];
